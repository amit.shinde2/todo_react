import React, { useState } from "react";
import { useContext, useEffect } from "react/cjs/react.development";
import AuthContext from "../context/auth-context";
import classes from "./Footer.module.css";

const Footer = (props) => {
  const authCtx = useContext(AuthContext);
  //   const [itemLeft, setItemLeft] = useState(0);
  //   useEffect(() => {
  //     props.strike((prevState) => {
  //       const arr = prevState.map((item) => {
  //         setItemLeft(itemLeft + 1);
  //         return { ...item };
  //       });
  //       return arr;
  //     });
  //   }, [itemLeft]);

  const allHandler = () => {
    authCtx.active = false;
    authCtx.completed = false;
    authCtx.allCap = true;

    props.strike((prevState) => {
      //just to render ToDo comp.
      const arr = prevState.map((item) => {
        return item;
      });
      return arr;
    });
  };

  const activeHandler = () => {
    authCtx.active = true;
    authCtx.completed = false;
    authCtx.allCap = false;

    props.setActiveText((prevState) => {
      const arr = prevState.filter((item) => {
        return !item.completed;
      });
      return arr;
    });
  };
  const completedHandler = () => {
    authCtx.active = false;
    authCtx.completed = true;
    authCtx.allCap = false;

    props.setCompletedText((prevState) => {
      const arr = prevState.filter((item) => {
        const dl = document.querySelectorAll("input[type=checkbox]");
        dl.forEach((item) => {
          item.checked = true;
        });
        return item.completed;
      });

      return arr;
    });
  };

  const completeClearHandler = () => {
    props.strike((prevState) => {
      const arr = prevState.filter((item) => !item.completed);
      
      return arr;
    });

    props.setCompletedText([]);
  };

  return (
    <React.Fragment>
      <div className={classes.form}>
        <div className={classes.formClass}>
          <div className={classes.input}>
            <ul>
              <li>{`${authCtx.itemLeft} Item Left`}</li>
              <li onClick={allHandler}>All</li>
              <li onClick={activeHandler}>Active</li>
              <li onClick={completedHandler}>Completed</li>
              <li onClick={completeClearHandler}>Clear</li>
            </ul>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Footer;
