import React, { useContext, useRef, useState } from "react";
import { useEffect } from "react/cjs/react.development";
import AuthContext from "../context/auth-context";
import classes from "./TodoItem.module.css";

const Form = (props) => {
  const radioRef = useRef();
  const deleteRef = useRef();
  const [dbl, setDbl] = useState(false);
  const [prevVal, setPrevVal] = useState("");

  const authCtx = useContext(AuthContext);
  var ch = true;

  const dblClick = (event) => {
    setDbl(true);
    setPrevVal(props.todoText.value);
  };

  const removerHandler = (index) => {
    // authCtx.itemLeft -= 1;

    props.strike((prevState) => {
      const arr = prevState.filter((item) => {
        return item.index != index;
      });
      return arr;
    });
  };

  useEffect(() => {
    if(authCtx.completed){
      const dl = document.querySelectorAll("input[type=checkbox]");
        dl.forEach((item) => {
          item.checked = true;
        });
    }
    
    if(authCtx.allCap){
    props.strike((prevState) => {
      const dl = document.querySelectorAll("input[type=checkbox]");
      const all = Array.from(dl);
      let inc = 0;
      const arr = prevState.map((item) => {
        if(!item.completed){
          all[inc].checked = false;
        }else{
          all[inc].checked = true;
        }

        inc += 1;
        return {...item}
      })
      // console.log("after", arr);
      console.log(arr);
      console.log(all);
      return arr;
    });
  }
  }, [authCtx.completed, authCtx.allCap])

 

  // const newNotes = props.todoText.filter((person) => person.index !== index);

  // props.strike(newNotes);

  const blurHandler = (event) => {
    // props.strike((prevState) => {
    //   const arr = prevState.filter((item) => {
    //    return item.index !== 1;
    //   })

    //   return arr;
    //  });
    if (event.target.value === "") {
      props.strike((prevState) => {
        const arr = prevState.filter((item) => {
          console.log(event.target.id);
          return item.value.length > 0;
        });
        console.log(arr);
        return arr;
      });
      authCtx.itemLeft -= 1;
    }
    setDbl(false);
  };

  const handleChange = (event) => {
    let val = "";

    event.target.addEventListener("keyup", function (event) {
      event.target.addEventListener("keyup", function (event) {
        if (event.key === "Escape") {
          setDbl(false);
        }
      });
      if (event.key === "Escape") {
        props.strike((prevState) => {
          const arr = prevState.map((item) => {
            val = item.value;
            if (item.index === Number(event.target.id)) {
              return { ...item, value: prevVal };
            } else {
              return { ...item };
            }
          });
          return arr;
        });
      }
    });

    if (dbl === true) {
      props.strike((prevState) => {
        const arr = prevState.map((item) => {
          val = item.value;
          if (item.index === Number(event.target.id)) {
            event.target.addEventListener("keyup", function (event) {
              if (event.key === "Enter") {
                if (event.target.value === "") {
                  removerHandler(item.index);
                }
                setDbl(false);
              }
            });
            return { ...item, value: event.target.value };
          } else {
            return { ...item };
          }
        });
        return arr;
      });
    }
  };

  const radioHandler = () => {
    props.todoText.completed = !props.todoText.completed;

    // if(props.todoText.completed){
    //   authCtx.itemLeft -= 1;
    // }else{
    //   authCtx.itemLeft += 1;
    // }
    props.strike((prevState) => {
      const arr = prevState.map((item) => {
        // document.getElementById(item.index).checked = true;
        authCtx.radio.push(radioRef.current.checked);
        if (item.completed) {
          // document.getElementById(item.index).checked = false;
          radioRef.current.checked = props.todoText.completed; //switch toggle of radio
          return { ...item, completed: true, checked: true };
        } else {
          radioRef.current.checked = props.todoText.completed;
          return { ...item, completed: false, checked: false };
        }
      });
      return arr;
    });
    // props.setActiveText((prevState) => {
    //   const arr = prevState.map((item) => {
    //     // document.getElementById(item.index).checked = true;
    //     if (item.completed) {
    //       // document.getElementById(item.index).checked = false;
    //       radioRef.current.checked = props.todoText.completed;
    //       return { ...item, completed: true, checked: true };
    //     } else {
    //       radioRef.current.checked = props.todoText.completed;
    //       return { ...item, completed: false, checked: false };
    //     }
    //   });
    //   return arr;
    // });

    // props.strike(prevState => {
    //   const arr = prevState.filter((item) => {
    //     {if(item.completed === true){
    //       console.log(item);
    //     }}

    //     return {...item}
    //   })

    //   return arr;
    // });

    // const val = document.getElementById(2);
    // console.log(val.id);
    // console.log(document.getElementById(2).checked);
  };

  const deleteHanlder = () => {
    props.strike((prevState) => {
      const arr = prevState.filter((item) => {
        return item.index != radioRef.current.id;
      });
      return arr;
    });

    authCtx.itemLeft -= 1;
  };

  return (
    <React.Fragment>
      <div className={classes.form}>
        <label>
          {props.todoText.completed && (
            <input
              type="text"
              id={props.unique}
              onChange={handleChange}
              onDoubleClick={dblClick}
              value={props.text}
              className={classes.inputst}
              onBlur={blurHandler}
            ></input>
          )}

          {!props.todoText.completed && (
            <input
              type="text"
              id={props.unique}
              onChange={handleChange}
              onDoubleClick={dblClick}
              value={props.text}
              className={`${classes.input}`}
              onBlur={blurHandler}
            ></input>
          )}
          {/* {!props.todoText.checked &&  */}
          <input
            type="checkbox"
            ref={radioRef}
            htmlFor={props.unique}
            id={props.unique}
            onClick={radioHandler}
            className={classes.radio}
          ></input>
          <a ref={deleteRef} onClick={deleteHanlder}>
            X
          </a>
        </label>
      </div>
    </React.Fragment>
  );
};

export default Form;
