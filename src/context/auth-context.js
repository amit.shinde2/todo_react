import React from 'react';
import store from './store';

const AuthContext = React.createContext({
    ...store
});

export default AuthContext;