const store = {
    todoText: '',
    checked:false,
    itemLeft:0,
    allCap:true,
    active:false,
    completed:false,
    activeHandler: () => {},
    radio: []
}

export default store;