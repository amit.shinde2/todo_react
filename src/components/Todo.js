import React, { useContext, useEffect, useState } from "react";
import TodoText from "./TodoText";
import TodoItem from "./TodoItem";
import classes from "./Todo.module.css";
import Footer from "./Footer";
import AuthContext from "../context/auth-context";

const Todo = () => {
  const [text, setText] = useState([]);
  const [activeText, setActiveText] = useState([]);
  const [completedText, setCompletedText] = useState([]);
  let [st, setSt] = useState(0);
  const authCtx = useContext(AuthContext);

  useEffect(() => {
    setActiveText(text);
    setCompletedText(text);

    


    // text.forEach((item) => {
    //   if (item.completed) {
    //     console.log("true");
    //      setSt(st-1);
    //   } else if (!item.completed) {
    //     console.log("false");
    //     setSt(st+1);  
    //   }
    // });

    setActiveText((prevState) => {
      const arr = prevState.filter((item) => !item.completed);
      // console.log("after", arr);

      return arr;
    });

    setCompletedText((prevState) => {
      const arr = prevState.filter((item) => item.completed);
      // console.log("after", arr);
      return arr;
    });

    // authCtx.radio.map((prevState) => {
    //   if(prevState.checked){
    //     console.log("ch",prevState);
    //     prevState.checked = true;
    //   }else{
    //     console.log("nch",prevState);
    //     prevState.checked = false;
    //   }
    //   });
  }, [text]);

  const addNote = (newNote) => {
    setText((prevState) => {
      return [...prevState, newNote];

    });

    // console.log("real", text);

    // setActiveText((prevState) => {
    //   return [...prevState, newNote];
    // });

    // setCompletedText((prevState) => {
    //   return [...prevState, newNote];
    // });
  };

  // text.map((item) => {
  //   if(item.completed){
  //     console.log("completed");
  //   }else{
  //     console.log("not completed");
  //   }
  // });

  // const shower = () => {
  //   setText((prevState) => {
  //     const arr = prevState.filter((item) => {
  //       if (item.completed === true) {
  //         return { ...item };
  //       }
  //     });

  //     return arr;
  //   });
  // };

  // console.log(activeText);

  return (
    <div className={classes.todo}>
      <TodoText onAdd={addNote} setTextProp={setText} todoText={text} />
      {authCtx.active &&
        !authCtx.completed &&
        !authCtx.allCap &&
        activeText.map((text) => (
          <TodoItem
            text={text.value}
            key={text.index}
            unique={text.index}
            todoText={text}
            strike={setText}
            activeText={activeText}
            setActiveText={setActiveText}
            completedText={completedText}
            setCompletedText={setCompletedText}
         
          />
        ))}
      {!authCtx.active &&
        !authCtx.completed &&
        authCtx.allCap &&
        text.map((text) => (
          <TodoItem
            text={text.value}
            key={text.index}
            unique={text.index}
            todoText={text}
            strike={setText}
            activeText={activeText}
            setActiveText={setActiveText}
            completedText={completedText}
            setCompletedText={setCompletedText}
          
          />
        ))}

      {!authCtx.active &&
        authCtx.completed &&
        !authCtx.allCap &&
        completedText.map((text) => (
          <TodoItem
            text={text.value}
            key={text.index}
            unique={text.index}
            todoText={text}
            strike={setText}
            activeText={activeText}
            setActiveText={setActiveText}
            completedText={completedText}
            setCompletedText={setCompletedText}
            
          />
        ))}

      <Footer
        todoText={text}
        strike={setText}
        activeText={activeText}
        setActiveText={setActiveText}
        completedText={completedText}
        setCompletedText={setCompletedText}
      />
      {/* <button onClick={shower}>hello</button> */}
    </div>
  );
};

export default Todo;
