import React, { useContext, useEffect, useRef, useState } from "react";
import AuthContext from "../context/auth-context";
import classes from "./TodoText.module.css";

const TodoText = (props) => {
  const [texts, setTexts] = useState({
    value: "",
    completed: false,
    index: 0,
    checked: false,
  });
  const [index, setIndex] = useState(0);
  const inputRef = useRef();
  
  const [check, setCheck] = useState(true);

  var ch = false;

  const authCtx = useContext(AuthContext);
  authCtx.checked = check;

  const handlChange = (event) => {
    const { value } = event.target;
    setTexts((prevState) => {
      return { ...prevState, value, index };
    });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    if (inputRef.current.value !== "") {
      setIndex(index + 1);
      authCtx.itemLeft += 1;
      props.onAdd(texts);

      const textData = document.getElementById("fname");
      textData.addEventListener("keyup", function (event) {
        if (event.key === "Enter") {
          event.preventDefault();
          event.target.value = "";
        }
      });
    }
  };

  const buttonHanlder = () => {

    props.setTextProp((prevState) => {   //strike logic here
      const arr = prevState.map((item) => {
        if (!item.completed) {
          ch = true;
        }
        return { ...item };
      });

      const dl = document.querySelectorAll("input[type=checkbox]");
      dl.forEach((item) => {
        item.checked = ch;
      });
      return arr;
    });

    // setAll(!all);

    // props.setTextProp((prevState) => {
    //   const arr = prevState.map((item) => {
    //     // document.getElementById(item.index).checked = check;
    //     return {...item, completed:all, checked:all}
    //   });
    //   return arr;
    // });
    // setCheck(!check);

    // props.todoText.map((item) => {
    //   if (!item.completed) {
    //     setIter(true);
    //   }
    // });

    
    props.setTextProp((prevState) => {
      const arr = prevState.map((item) => {
        // document.getElementById(item.index).checked = true;
        // document.getElementById(item.index).checked = false;
        return { ...item, completed: ch, checked: ch };
        //implment coorectly
      });
      // console.log(arr);
      return arr;
    });
    setCheck(!check);
    // } else {
    //   props.setTextProp((prevState) => {
    //     const arr = prevState.map((item) => {
    //       // document.getElementById(item.index).checked = true;
    //       // document.getElementById(item.index).checked = false;
    //       return { ...item, completed: true, checked: true };
    //       //implment coorectly
    //     });
    //     // console.log(arr);
    //     return arr;
    //   });
    //   // setCheck(!check);
    //   setIter(false);
    // }
    // console.log(document.getElementsByClassName("TodoItem_radio__Kgt--"));
  };

  return (
    <React.Fragment>
      <div className={classes.form}>
        <form onSubmit={submitHandler}>
          <input
            type="text"
            id="fname"
            ref={inputRef}
            name="fname"
            placeholder="What needs to be done?"
            className={classes.input}
            onChange={handlChange}
          ></input>
          <button
            id="css"
            type="button"
            onClick={buttonHanlder}
            className={classes.radio}
          ></button>
        </form>
      </div>
    </React.Fragment>
  );
};

export default TodoText;
