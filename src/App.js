import React from 'react';
import './App.css';
import Todo from './components/Todo';
import AuthContext from './context/auth-context';
import store from './context/store';

function App() {
  return (
   <AuthContext.Provider value={store}>
    <p>todos</p>
    <Todo/>
   </AuthContext.Provider>
  );
}

export default App;
